#include <stdio.h>
#include <string.h>
// strings_equal
#include "../types/string.c"

typedef struct Array {
  int size;
  char * type;
  void * arr;
} Array;

Array map(void * (*fp)(void *), Array arr) {
  int i;
  for(i = 0; i < arr.size; ++i)
    // TODO: add other cases for float, etc. here
    if(strings_equal(arr.type, "int"))
      ((int *) arr.arr)[i] = fp(((int *) arr.arr)[i]);
  return arr;
}

int sqr(int n) {
  return n * n;
}

void print_array(Array arr) {
  int i;
  printf("[");

  for(i = 0; i < arr.size; ++i) {
    if(strings_equal(arr.type, "int"))
      printf("%d", ((int *) arr.arr)[i]);
    else if(strings_equal(arr.type, "string"))
      printf("%s", ((char **) arr.arr)[i]);
    if(i + 1 != arr.size)
      printf(", ");
  }
  puts("]");
}

int main(void) {
  int arr[5] = { 1, 2, 3, 4, 5 };
  Array a1  = { .size = 5, .type = "int", .arr = arr };
  print_array(map(&sqr, a1));

  char * arr1[3] = { "foos", "bar fjaja", "baz" };
  Array a2  = { .size = 3, .type = "string", .arr = arr1 };
  print_array(a2);

  return 0;
}
